#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

GE_REPO="${GE_REPO:-https://github.com/GloriousEggroll/proton-ge-custom}"
GE_PATH="${GE_PATH:-${HOME}/.steam/root/compatibilitytools.d}"
GE_KEEP="${GE_KEEP:-3}"
GE_RESTART="${GE_RESTART:-1}"
GE_STEAM="${GE_STEAM:-/usr/games/steam}"

## Logger function(s)
# Detect if we are being run interactively
# If we are then echo our output
# If not then send to syslog
loginfo() {
    if [[ -n "${TERM}" && "${TERM}" != "dumb" ]]; then
        echo "${1}"
    else
        logger -p "local3.info" "${1}"
    fi
}

logerror() {
    if [[ -n "${TERM}" && "${TERM}" != "dumb" ]]; then
        echo "${1}"
    else
        logger -p "local3.err" "${1}"
    fi
}

get_latest_release() {
    curl -Ls "${GE_REPO//github.com/api.github.com/repos}/releases/latest" \
        | grep tag_name \
        | cut -d '"' -f 4
}

create_dest() {
    if [[ ! -d "${GE_PATH}" ]]; then
        if ! mkdir -p "${GE_PATH}"; then
            logerror "Failed to create ${GE_PATH}"
            exit 1
        fi
    fi
}

check_installed() {
    if [[ -z "${1}" ]]; then
        logerror "Unable to find latest version"
        exit 1
    fi

    if [[ -d "${GE_PATH}/Proton-${1}" ]]; then
        loginfo "Proton-${version} already installed"
        exit 0
    else
        loginfo "Proton-${version} not installed"
    fi
}

download_proton() {
    loginfo "Downloading Proton-${1}"
    for i in tar.gz sha512sum; do
        if ! curl -Lso "/tmp/Proton-${1}.${i}" \
            "${GE_REPO}/releases/download/${1}/Proton-${1}.${i}"; then

            logerror "Failed to download Proton-${1}.${i}. Exiting"
            exit 1
        fi
    done
}

install_proton() {
    loginfo "Installing Proton-${1}"
    local release
    local download
    release=$(awk '{print $1}' "/tmp/Proton-${1}.sha512sum")
    download=$(sha512sum "/tmp/Proton-${1}.tar.gz" | awk '{print $1}')

    loginfo "Checking sha512sum of download"
    if [[ "${release}" != "${download}" ]]; then
        logerror "sha512sum did not match. Exiting."
        exit 1
    fi

    loginfo "Extracting Proton-${1}"
    if ! tar xf "/tmp/Proton-${1}.tar.gz" -C "${GE_PATH}"; then
        logerror "Extracting Proton-${1}.tar.gz to ${GE_PATH} failed"
        exit 1
    fi
}

restart_steam() {
    if [[ "${GE_RESTART}" -eq 1 ]]; then
        if [[ "$(pgrep steam)" != "" ]]; then
            loginfo "Restarting Steam"
            pkill -TERM steam
            while pgrep steam >/dev/null; do
                loginfo "Waiting for Steam to die"
                sleep 1
            done
            loginfo "Steam Stopped"
            sleep 5
            loginfo "Starting Steam"
            /usr/bin/nohup "${GE_STEAM}" </dev/null &>/dev/null &
        else
            loginfo "Steam not running"
        fi
    fi
}

cleanup_old() {
    if [[ "${GE_KEEP}" -ne 0 ]]; then
        loginfo "Removing all but the last ${GE_KEEP} versions of Proton"
        find "${GE_PATH}" \
            -mindepth 1 \
            -maxdepth 1 \
            -type d \
            -name "Proton-*GE*" \
            | sort -V \
            | head -n -"${GE_KEEP}" \
            | xargs rm -rf --
    fi
}

cleanup_tmp() {
    loginfo "Cleaning up temp files"
    rm -rf /tmp/Proton-*
}

main() {
    trap cleanup_tmp EXIT

    create_dest

    loginfo "Checking for latest Proton GE release"
    local version
    local installed
    version=$(get_latest_release)

    check_installed "${version}"
    download_proton "${version}"
    install_proton "${version}"
    cleanup_old
    restart_steam
}

main
