# Proton GloriousEggroll Updater

Script to automatically install the latest version of [proton-ge-custom](
https://github.com/GloriousEggroll/proton-ge-custom) for use with Steam.

It will also remove all but the latest 3 (configurable) versions of Proton GE.

## Basic Usage

`./proton-ge-update`

## Options

This script uses ENVARS for it's options.

`GE_REPO` Repo to grab releases from  
Default: `https://github.com/GloriousEggroll/proton-ge-custom`

`GE_PATH` Path to install Proton GE to  
Default: `${HOME}/.steam/root/compatibilitytools.d`

`GE_KEEP` Number of releases to keep. Set to `0` to diable  
Default: `3`

`GE_RESTART` Restart Steam if new version of Proton installed  
Default: `true`

`GE_STEAM` Path to steam binary  
Default: `/usr/games/steam`

## Examples

Keep 5 latest versions and don't restart Steam.

```
GE_KEEP=5 GE_RESTART=0 ./proton-ge-update
```

# Cron

Included in the repo is a script to install a cronjob to auto update/restart steam at 4am every day.

`./install-cron.sh`

## Options

`GE_BIN` Path to your bin directory  
Default: ~/bin

`GE_CRON` Cron pattern for cronjob  
Default: `0 4 * * *` (4am daily)
