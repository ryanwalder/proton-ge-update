#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

GE_BIN=${GE_BIN:-${HOME}/bin}
GE_CRON="0 4 * * *"

cp proton-ge-update "${GE_BIN}/proton-ge-update"
chmod 700 "${GE_BIN}/proton-ge-update"

crontab -l > /tmp/cron
sed -i '/proton-ge-update/d' /tmp/cron
echo "${GE_CRON} DISPLAY=:0 ${GE_BIN}/proton-ge-update" >> /tmp/cron
crontab /tmp/cron
rm -rf /tmp/cron
